## Vue.js 源码剖析-响应式原理、虚拟 DOM、模板编译和组件化

### 一、简答题

#### 1、请简述 Vue 首次渲染的过程。
1. Vue初始化、实例、静态成员

   - 首先进行vue初始化，初始化实例、静态成员
2. new Vue()
   - 调用Vue的构造函数，在构造函数中调用this._init()方法
3. this._init()
   - 项目入口，最终调用`this.$mount()`
4. this.$mount()，有两个
   - 第一个`this.$mount()` 把模板转换成render函数，为`src/platform/web/entry-runtime-with-compiler.js`中定义的
     1. 先判断是否传入了`render`选项，如果没有传入则把模板转换成`render`函数
     2. 通过complieToFunction() 函数生成render()渲染函数
     3. 把`render`函数编译好之后，存储到`options.render`中
   - 第二个`this.$mount()` 是`src/platforms/web/runtime/index.js`中的方法，因为如果是运行时版本是不会走`entry-runtime-with-compiler.js`这个入口重新获取el，所以如果是运行版本那我们会在`runtime/index.js`的`$mount()`中重新获取el
5. mountComponent(this,el) 
   - 调用`mountComponent(this,el)`进行初始化vue生命周期的相关变量，在mountComponent()中，先判断是否有render选项，如果没有但传入了模板并且当前在开发环境会警告运行是版本不支持编译器，接着触发`beforeMount`中的钩子函数
6. 定义updateComponent ， updateComponent 中定义了两个 定义了`_render`和`_update`
   - `_render`  生成虚拟dom
   - `_update`   将虚拟dom转换为真实dom 并挂在到页面上
7. 创建Watcher实例
   - 再创建`Watcher` 时传递了`updateComponent` 这个函数，这个函数是在`Watcher`内部调用的，创建完成`Watcher`后会调用一次`get()`,在get方法中，会调用`updateComponent()`
8. 触发生命周期狗子函数`mounted`挂载结束并返回vue实例

#### 2、请简述 Vue 响应式原理。

Vue2.x使用`Object.defineProperty`,3.x使用ES6的`Proxy`

- initState() -> initData() -> observe()
  - initState vm 状态的初始化
  - initData  vm 数据的初始化
  - observe 负责为每一个 Object 类型的 value 创建一个 observer 实例
- Observer(value)
  - 判断value是否是对象
  - 判断value是否有`__ob__`(observer对象),如果有直接返回，如果没有创建observer对象
  - 返回observer对象
- Observer
  - 给vlaue对象定义不可枚举属性: `__ob__`属性，记录observer对象
  - 分数组和对象不同处理方法，对象响应式调用walk方法遍历对象所有属性为每一个属性调用defifineReactive() 方法设置getter/setter
- defineReactive()
  - 为一个对象定义一个响应式的属性，每一个属性对应一个 dep 对象
  - 如果该属性的值是对象，继续调用 observe
  - 定义getter
    - 收集依赖
    - 返回属性值
  - 定义setter
    - 保存新值
    - 如果新值是对象，调用observer
    - 派发更新，调用dep.notify()
- 收集依赖
  - 在watcher对象的get方法中调用pushTarget 记录 Dep.target 属性
  - 访问data中的成员的时候收集依赖，defineReactive 的 getter 中收集依赖
  - 把属性对应的 watcher 对象添加到 dep 的 subs 数组中
  - 给 childOb 收集依赖，目的是子对象添加和删除成员时发送通知
- Wacher
  - dep.notify() 在调用 wacher 对象的 update()
  - queueWatcher() 判断 wacher 是否被处理，如果没有的话添加到 queue 队列中，并调用 flushSchedulerQueue()
  - flushSchedulerQueue
    - 触发 beforeUpdate 钩子函数
    - 调用 wacher.run()
      - run()
      - get()
      - getter()
      - updateComponent
    - 清空上一次的依赖
    - 触发 actived 钩子函数
    - 触发 updated 钩子函数

#### 3、请简述虚拟 DOM 中 Key 的作用和好处。

- 作用：以便它能够跟踪每个节点的身份，在进行比较的时候，会基于 key 的变化重新排列元素顺序。从而重用和重新排序现有元素，并且会移除 key 不存在的元素。方便让 vnode 在 diff 的过程中找到对应的节点，然后成功复用。
- 好处： 可以减少 dom 的操作，减少 diff 和渲染所需要的时间，提升了性能。

#### 4、请简述 Vue 中模板编译的过程。

1. 解析
   - 解析器将模板解析为抽象语树 AST
   - 结构化指令的处理
     - v-if 最终生成单元表达式
     - 最终调用 genIfConditions 生成三元表达式
2. 优化
   - 优化抽象语法树，检测子节点中是否是纯静态节点
   - 静态子节点:
     - 提升为常量，重新渲染的时候不在重新创建节点
     - 在 patch 的时候直接跳过静态子树
3. 生成
   - 把字符串转换成函数